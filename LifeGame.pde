import controlP5.*;

public class LifeGame extends PApplet {

  final int NUMBER_OF_COLUMNS = 50;
  final int CELL_SIZE = 15;
  final int MENU_PANEL_HEIGHT = 50;

  final int WIDTH = NUMBER_OF_COLUMNS *  CELL_SIZE;
  final int HEIGHT = NUMBER_OF_COLUMNS *  CELL_SIZE + MENU_PANEL_HEIGHT;

  final int WHITE = color(255, 255, 255);
  final int BLACK = color(0, 0, 0);

  final int[] conditionForNewCells = {3};
  final int[] conditionForBlackCellsSurvive = {2, 3};

  ControlP5 cp5;
  int probabilityToFillACell = 50;

  Cell[][] grid = new Cell[NUMBER_OF_COLUMNS][NUMBER_OF_COLUMNS];

  void settings() {
    size(WIDTH, HEIGHT);
  }

  void setup() {
    background(WHITE);
    noStroke();

    grid = createRandomGrid();
    cp5 = new ControlP5(this);
    cp5.addNumberbox("numberbox")
      .setPosition(0, 0)
      .setSize(150, 20)
      .setScrollSensitivity(1)
      .setValue(probabilityToFillACell)
      .setMin(0)
      .setMax(100)
      .setColorLabel(BLACK)
      .setLabel("Probability to fill a cell");
  }

  void draw() {
    fillRandomCell();
    Cell[][] nextStepGrid = lifeNextStepGrid(grid);
    displayGrid(nextStepGrid);
    grid = nextStepGrid;
  }

  void displayGrid(Cell[][] grid) {
    for (int i = 0; i < NUMBER_OF_COLUMNS; i ++) {
      for (int j = 0; j < NUMBER_OF_COLUMNS; j ++) {
        grid[i][j].display();
      }
    }
  }

  Cell[][] createRandomGrid() {
    Cell[][] grid = new Cell[NUMBER_OF_COLUMNS][NUMBER_OF_COLUMNS];
    for (int i = 0; i < NUMBER_OF_COLUMNS; i ++) {
      for (int j = 0; j < NUMBER_OF_COLUMNS; j ++) {
        int randInt = int(random(100));
        int fillingColor = randInt < probabilityToFillACell ? BLACK : WHITE;
        grid[i][j] = new Cell(i, j, fillingColor);
      }
    }
    return grid;
  }

  Cell[][] lifeNextStepGrid(Cell[][] grid) {
    Cell[][] newGrid = new Cell[NUMBER_OF_COLUMNS][NUMBER_OF_COLUMNS];
    for (int i = 0; i < NUMBER_OF_COLUMNS; i ++) {
      for (int j = 0; j < NUMBER_OF_COLUMNS; j ++) {
        int fillingColor = WHITE;
        int numberOfAliveCells = getNumberOfFilledNeighbours(grid, i, j);
        if (grid[i][j].isWhite()) {
          for (int num : conditionForNewCells) {
            if (numberOfAliveCells == num) {
              fillingColor = BLACK;
              break;
            }
          }
        } else {
          for (int num : conditionForBlackCellsSurvive) {
            if (numberOfAliveCells == num) {
              fillingColor = BLACK;
              break;
            }
          }
        }
        newGrid[i][j] = new Cell(i, j, fillingColor);
      }
    }
    return newGrid;
  }

  int getNumberOfFilledNeighbours(Cell[][] grid, int x, int y) {
    int number = 0;
    Cell[] neighbours = getNeighbours(grid, x, y);
    for (int i = 0; i < neighbours.length; i++) {
      if (neighbours[i].isBlack()) {
        number++;
      }
    }
    return number;
  }

  Cell[] getNeighbours(Cell[][] grid, int x, int y) {
    Cell[] neighbours = new Cell[8];
    x += NUMBER_OF_COLUMNS;
    y += NUMBER_OF_COLUMNS;
    neighbours[0] = grid[(x - 1) % NUMBER_OF_COLUMNS][(y - 1) % NUMBER_OF_COLUMNS];
    neighbours[1] = grid[(x - 1) % NUMBER_OF_COLUMNS][y % NUMBER_OF_COLUMNS];
    neighbours[2] = grid[(x - 1) % NUMBER_OF_COLUMNS][(y + 1) % NUMBER_OF_COLUMNS];
    neighbours[3] = grid[x % NUMBER_OF_COLUMNS][(y - 1) % NUMBER_OF_COLUMNS];
    neighbours[4] = grid[x % NUMBER_OF_COLUMNS][(y + 1) % NUMBER_OF_COLUMNS];
    neighbours[5] = grid[(x + 1) % NUMBER_OF_COLUMNS][(y - 1) % NUMBER_OF_COLUMNS];
    neighbours[6] = grid[(x + 1) % NUMBER_OF_COLUMNS][y % NUMBER_OF_COLUMNS];
    neighbours[7] = grid[(x + 1) % NUMBER_OF_COLUMNS][(y + 1) % NUMBER_OF_COLUMNS];
    return neighbours;
  }

  void fillRandomCell() {
    int x = int(random(NUMBER_OF_COLUMNS));
    int y = int(random(NUMBER_OF_COLUMNS));
    grid[x][y].setBlack();
    grid[x][y].display();
  }

  int getRandomColor() {
    int r = int(random(255));
    int g = int(random(255));
    int b = int(random(255));
    return color(r, g, b);
  }

  void numberbox(int aValue) {
    probabilityToFillACell = aValue;
    grid = createRandomGrid();
  }

  public class Cell {
    private final int x;
    private final int y;

    private int aColor;

    public Cell(int x, int y, int aColor) {
      this.x = x;
      this.y = y;
      this.aColor = aColor;
    }

    public void display() {
      fill(aColor);
      rect(x * CELL_SIZE, y * CELL_SIZE + MENU_PANEL_HEIGHT, CELL_SIZE, CELL_SIZE);
    }

    public void setBlack() {
      aColor = BLACK;
    }

    public void setWhite() {
      aColor = WHITE;
    }

    public void setRandomColor() {
      aColor = getRandomColor();
    }

    public boolean isBlack() {
      return aColor == BLACK;
    }

    public boolean isWhite() {
      return aColor == WHITE;
    }
  }
}

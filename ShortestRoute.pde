import controlP5.*; //<>// //<>//
import java.util.List;
import java.util.Collections;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.Collectors;

public class ShortestRoute extends PApplet {

  final int WHITE = color(255, 255, 255);
  final int BLACK = color(0, 0, 0);
  final int SOLID_PINK = color(142, 59, 70);
  final int CANDY_PINK = color(224, 119, 125);
  final int GREEN_YELLOW_CRAYOLA = color(225, 221, 143);
  final int STEEL_BLUE = color(76, 134, 168);
  final int JET = color(51, 51, 51);

  final int UP_PANEL_HEIGHT = 0;
  final int BOTTOM_PANEL_HEIGHT = 40;
  final int LEFT_PANEL_WIDTH = 100;

  final int WINDOW_WIDTH = 1200;
  final int WINDOW_HEIGHT = 900;

  final int WIDTH = WINDOW_WIDTH + LEFT_PANEL_WIDTH;
  final int HEIGHT = UP_PANEL_HEIGHT + WINDOW_HEIGHT + BOTTOM_PANEL_HEIGHT;

  final int TOWN_DIAMETER = 10;

  final float START_TEMPERATURE = 1.;
  final float END_TEMPERATURE = 0.001;
  final float TEMPERATURE_COEFF = 0.999;

  int NUMBER_OF_TOWNS = 25;

  final int BACKGROUND_COLOR = SOLID_PINK;
  final int WINDOW_COLOR = CANDY_PINK;
  final int TOWN_COLOR = GREEN_YELLOW_CRAYOLA;
  final int TOWN_NAME_COLOR = BLACK;
  final int ROUTE_COLOR = JET;

  Town[] towns;
  float[][] destinations;
  Textlabel routeLengthTextLabel;
  boolean annealingStarted = false;
  Route aRoute;
  float currentTemperature = START_TEMPERATURE;
  Textfield routeTextField;
  Textfield routeSwap1Field;
  Textfield routeSwap2Field;

  long totalIterations;
  long lastIterations;
  long maxLastIterations;

  ControlP5 cp5;

  void settings() {
    size(WIDTH, HEIGHT);
  }

  void setup() {
    noStroke();
    background(BACKGROUND_COLOR);


    cp5 = new ControlP5(this);
    cp5.addNumberbox("NUMBER_OF_TOWNS")
      .setPosition(0, 0)
      .setSize(100, 20)
      .setScrollSensitivity(1)
      .setValue(NUMBER_OF_TOWNS)
      .setMin(4)
      .setMax(200)
      .setColorLabel(WHITE)
      .setLabel("Number of towns");
    cp5.addButton("generateNewMap")
      .setPosition(0, 40)
      .setSize(100, 20)
      .setLabel("Generate new map");
    routeLengthTextLabel = cp5.addTextlabel("routeLength")
      .setPosition(0, 60)
      .setColorValue(0xffffff00)
      .setFont(createFont("Arial", 16))
      ;
    routeTextField = cp5.addTextfield("routeTextValue")
      .setPosition(LEFT_PANEL_WIDTH, WINDOW_HEIGHT)
      .setSize(500, 20)
      .setLabel("")
      .setFont(createFont("arial", 12))
      .setAutoClear(false)
      ;
    routeSwap1Field = cp5.addTextfield("routeSwap1Value")
      .setPosition(LEFT_PANEL_WIDTH, WINDOW_HEIGHT + 20)
      .setSize(20, 20)
      .setFont(createFont("arial", 12))
      .setAutoClear(false)
      ;
    routeSwap2Field = cp5.addTextfield("routeSwap2Value")
      .setPosition(LEFT_PANEL_WIDTH + 20 + 5, WINDOW_HEIGHT + 20)
      .setSize(20, 20)
      .setFont(createFont("arial", 12))
      .setAutoClear(false)
      ;
    cp5.addButton("applyChanges")
      .setPosition(LEFT_PANEL_WIDTH + 500 + 5, WINDOW_HEIGHT)
      .setSize(80, 20)
      .setLabel("Apply");
    cp5.addButton("applySwap")
      .setPosition(LEFT_PANEL_WIDTH + 20 + 5 + 20 + 5, WINDOW_HEIGHT + 20)
      .setSize(80, 20)
      .setLabel("Apply");
    cp5.addButton("startAnnealing")
      .setPosition(0, 80)
      .setSize(100, 20)
      .setLabel("Start annealing");
    generateNewMap();
  }

  void draw() {
    if (annealingStarted) {
      annealingNextStep();
      redrawWindow();
      if (currentTemperature <= END_TEMPERATURE) {
        maxLastIterations = Math.max(maxLastIterations, lastIterations);
        System.out.println( totalIterations +" "+ lastIterations + " " + maxLastIterations);
        annealingStarted = false;
      }
    }
  }

  void startAnnealing() {
    setRoute(createRandomRoute());
    lastIterations = 0;
    totalIterations = 0;
    maxLastIterations = 0;
    currentTemperature = START_TEMPERATURE;
    annealingStarted = true;
  }

  void annealingNextStep() {
    lastIterations++;
    totalIterations++;
    Route potentialNewRoute = aRoute.mutate();
    float deltaE = potentialNewRoute.getLength() - aRoute.getLength();
    if (deltaE < 0.) {
      if (abs(deltaE) > 0.4) {
        System.out.println( totalIterations +" "+ lastIterations + " " + maxLastIterations);
        System.out.println("ERROR: " + deltaE);
        maxLastIterations = Math.max(maxLastIterations, lastIterations);
        lastIterations = 0;
      }
      setRoute(potentialNewRoute);
    } else {
      float probability = calculateProbabilityToTransfer(deltaE);
      float randFloat = random(1.);
      if (randFloat < probability) {
        setRoute(potentialNewRoute);
      }
    }
    currentTemperature *= TEMPERATURE_COEFF;
  }

  void applyChanges() {
    setRoute(aRoute.fromString(routeTextField.getText()));
    redrawWindow();
  }

  void applySwap() {
    Integer id1 = Integer.parseInt(routeSwap1Field.getText());
    Integer id2 = Integer.parseInt(routeSwap2Field.getText());
    setRoute(aRoute.withSwappedIds(id1, id2));
    redrawWindow();
  }

  void setRoute(Route aRoute) {
    this.aRoute = aRoute;
    routeTextField.setValue(aRoute.toString());
  }

  float calculateProbabilityToTransfer(float deltaE) {
    float expDeltaE = 1. - exp(-deltaE);
    return exp(-expDeltaE / currentTemperature);
  }

  void drawWindow() {
    fill(WINDOW_COLOR);
    rect(LEFT_PANEL_WIDTH, UP_PANEL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT);
  }

  void generateTowns() {
    towns = new Town[NUMBER_OF_TOWNS];
    for (int i = 0; i < NUMBER_OF_TOWNS; i++) {
      int randX = int(random(WINDOW_WIDTH));
      int randY = int(random(WINDOW_HEIGHT));
      towns[i] = new Town(i, randX, randY);
    }
    calculateDestinations();
  }

  void calculateDestinations() {
    destinations = new float[NUMBER_OF_TOWNS][NUMBER_OF_TOWNS];
    for (int i = 0; i < NUMBER_OF_TOWNS - 1; i++) {
      Town town1 = towns[i];
      for (int j = i + 1; j < NUMBER_OF_TOWNS; j++) {
        Town town2 = towns[j];
        destinations[i][j] = destinations[j][i] = getDestinationBetween(town1, town2);
      }
    }
  }

  Route createRandomRoute() {
    List<Integer> integers =
      IntStream.range(0, NUMBER_OF_TOWNS)
      .boxed()
      .collect(Collectors.toList());

    Collections.shuffle(integers);

    return new Route(integers);
  }

  void redrawWindow() {
    background(BACKGROUND_COLOR);
    drawWindow();
    aRoute.display();
    for (Town aTown : towns) {
      aTown.display();
    }
    routeLengthTextLabel.setText(String.valueOf(Math.round(aRoute.getLength())));
  }

  void generateNewMap() {
    background(BACKGROUND_COLOR);
    drawWindow();
    generateTowns();
    setRoute(createRandomRoute());
    aRoute.display();
    for (Town aTown : towns) {
      aTown.display();
    }
    routeLengthTextLabel.setText(String.valueOf(Math.round(aRoute.getLength())));
  }

  float getDestinationBetween(Town townFrom, Town townTo) {
    float cathetX = abs(townFrom.getX() - townTo.getX());
    float cathetY = abs(townFrom.getY() - townTo.getY());
    return sqrt(pow(cathetX, 2.) + pow(cathetY, 2.));
  }

  class Town {
    private int id;
    private int x;
    private int y;

    public Town(int id, int x, int y) {
      this.id = id;
      this.x = x;
      this.y = y;
    }

    public void display() {
      fill(TOWN_COLOR);
      circle(x + LEFT_PANEL_WIDTH, y + UP_PANEL_HEIGHT, TOWN_DIAMETER);
      fill(TOWN_NAME_COLOR);
      textAlign(CENTER);
      text("#" + id, x + LEFT_PANEL_WIDTH, y + UP_PANEL_HEIGHT + TOWN_DIAMETER * 1.2);
    }

    public int getX() {
      return x;
    }


    public int getY() {
      return y;
    }
  }

  class Route {
    private List<Integer> routeWay;

    private final float routeLength;

    public Route(List<Integer> routeWay) {
      this.routeWay = routeWay;
      this.routeLength = calculateLength();
    }

    public void display() {
      stroke(ROUTE_COLOR);
      for (int i = 0; i < NUMBER_OF_TOWNS; i++) {
        int idFrom = routeWay.get(i);
        int idTo = i == NUMBER_OF_TOWNS - 1 ? routeWay.get(0) : routeWay.get(i + 1);
        Town townFrom = towns[idFrom];
        Town townTo = towns[idTo];

        line(townFrom.getX() + LEFT_PANEL_WIDTH, townFrom.getY() + UP_PANEL_HEIGHT, townTo.getX() + LEFT_PANEL_WIDTH, townTo.getY() + UP_PANEL_HEIGHT);
      }
      noStroke();
    }

    private float calculateLength() {
      float len = 0.;
      for (int i = 0; i < NUMBER_OF_TOWNS; i++) {
        int idFrom = routeWay.get(i);
        int idTo = i == NUMBER_OF_TOWNS - 1 ? routeWay.get(0) : routeWay.get(i + 1);
        len += destinations[idFrom][idTo];
      }
      return len;
    }

    public float getLength() {
      return routeLength;
    }

    public Route mutate() {
      List<Integer> copyRouteWay = new ArrayList<>(routeWay);
      int mode = int(random(4));
      if (mode == 0) {
        shiftElement(copyRouteWay);
      } else if (mode == 1) {
        swapElems(copyRouteWay);
      } else if (mode == 2) {
        swapReversed(copyRouteWay);
      } else {
        swapReversed(copyRouteWay);
        swapReversed(copyRouteWay);
      }
      return new Route(copyRouteWay);
    }

    private void shiftElement(List<Integer> aList) {
      int randomIndex = int(random(NUMBER_OF_TOWNS));
      Integer randomElem = aList.remove(randomIndex);
      int newRandomIndex = int(random(NUMBER_OF_TOWNS));
      aList.add(newRandomIndex, randomElem);
    }

    private void swapElems(List<Integer> aList) {
      int quantity = int(random(2, 2));
      Integer[] randomIndexes = getRandomNumbers(quantity);
      for (int i = 0; i < quantity - 1; i++) {
        Collections.swap(aList, randomIndexes[i], randomIndexes[i+1]);
      }
    }

    private Integer[] getRandomNumbers(int count) {
      List<Integer> integers =
        IntStream.range(0, NUMBER_OF_TOWNS)
        .boxed()
        .collect(Collectors.toList());

      Collections.shuffle(integers);

      //return integers.stream().limit(count).toArray(Integer[]::new);
      //return integers.stream().limit(count).toArray(Integer[]::new);
      //return integers.stream().limit(count).toArray(Integer[]::new);
      return integers.stream().limit(count).toArray(Integer[]::new);
    }

    @Override
      public String toString() {
      return routeWay.toString();
    }

    public Route fromString(String str) {
      String strWithoutBrackets = str.substring(1, str.length() - 1);
      List<Integer> aList = Stream.of(strWithoutBrackets.split(","))
        .map(x -> x.replaceAll("\\s+", ""))
        .map(x -> Integer.parseInt(x))
        .collect(Collectors.toList());
      return new Route(aList);
    }

    public Route withSwappedIds(Integer id1, Integer id2) {
      Integer inx1 = findIndexOf(id1);
      Integer inx2 = findIndexOf(id2);
      return withSwappedIndexes(inx1, inx2);
    }

    public Route withSwappedIndexes(Integer inx1, Integer inx2) {
      List<Integer> copyRouteWay = new ArrayList<>(routeWay);
      swapReversed(copyRouteWay, inx1, inx2);
      return new Route(copyRouteWay);
    }

    public void swapReversed(List<Integer> aList) {
      Integer[] randomIndexes = getRandomNumbers(2);
      swapReversed(aList, randomIndexes[0], randomIndexes[1]);
    }

    private void swapReversed(List<Integer> aList, Integer inx1, Integer inx2) {
      Integer minInx = min(inx1, inx2);
      Integer maxInx = max(inx1, inx2);
      Collections.reverse(aList.subList(minInx + 1, maxInx + 1));
    }

    private Integer findIndexOf(Integer value) {
      for (int i = 0; i < routeWay.size(); i++) {
        if (value.equals(routeWay.get(i))) {
          return i;
        }
      }
      return -1;
    }
  }
}
